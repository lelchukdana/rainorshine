import React, {useState, useEffect} from "react";
import '../App.css'

function Greeting() {
    const [greeting, setGreeting] = useState('');


useEffect(() => {
    const currentTime = new Date();
    const hours = currentTime.getHours();

    if (hours >= 5 && hours < 12) {
      setGreeting('Good Morning');
    } else if (hours >= 12 && hours < 17) {
      setGreeting('Good Afternoon');
    } else {
      setGreeting('Good Evening');
    }

  }, []);

  return (
  <div>
    <h1>{greeting}</h1>
  </div>
  )
};

export default Greeting;
