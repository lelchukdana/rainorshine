import React, {useState, useEffect} from "react";

function Background() {
    const [background, setBackground] = useState('');


    useEffect(() => {
        const currentTime = new Date();
        const hours = currentTime.getHours();

        if (hours >= 5 && hours < 12) {
          setBackground('morning')
        } else if (hours >= 12 && hours < 17) {
          setBackground('afternoon')
        } else {
          setBackground('evening')
        }

      }, []);

return (
    <div className={`background ${background}`}></div>
)

};

export default Background;
