import React, { useEffect, useState }from 'react';
import axios from 'axios';
import Greeting from '../components/Greeting';
import {FiSunrise, FiSunset} from 'react-icons/fi'
import {BsSearch, BsArrowRight} from 'react-icons/bs'
import '../App.css'

function Home() {
    const [city, setCity] = useState('');
    const [weatherData, setWeatherData] = useState([]);
    const [error, setError] = useState(null)
    const [isCelsius, setIsCelsius] = useState(false);

    const apiKey = process.env.REACT_APP_API_KEY;

    const fetchWeather = async (e) => {
      e.preventDefault();
      setError(null);
      try {
        const response = await axios.get(
            `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`
        );
        console.log('API Response:', response.data);
        setWeatherData(response.data);
        setCity('')
        } catch (error) {
            setError('Oh no, City not found! Please enter a valid city name');
        }
    };

    // Conversion of unix timestamps for sunrise and sunset
    const formatSunTime = (timestamp) => {
        const date = new Date(timestamp * 1000); // Convert to milliseconds
        return date.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}); // Format time as hh:mm
      };

    // Conversion from Fahrenheit to Celsius
      const toggleTemperature = () => {
        setIsCelsius(!isCelsius);
      };

    return (
        <div className="home">
          <div className="greeting">
            <Greeting />
          </div>
          <div className="search">
            <input
              className="input"
              type="text"
              value={city}
              onChange={(e) => setCity(e.target.value)}
              placeholder="Search your city"
            />
            <button className='button' onClick={fetchWeather}>
                <BsSearch />
            </button>
          </div>
          {weatherData.name && (
            <div className="container">
              <div className="top">
                <div className="location">
                  <p>{weatherData.name}</p>
                </div>
                <div className="temperature">
                <h1>
                {isCelsius
                    ? (weatherData.main.temp - 273.15).toFixed(1) + "°C"
                    : ((weatherData.main.temp - 273.15) * 9 / 5 + 32).toFixed(1) + "°F"}
                </h1>
                </div>
                <div className="description bold">
                  {weatherData.weather[0].description}
                </div>
              </div>
              <div className="bottom">
                <div className="feels-like">
                  <p>Feels like</p>
                  <p className="bold">
                  {isCelsius
                    ? (weatherData.main.temp - 273.15).toFixed(1) + "°C"
                    : ((weatherData.main.temp - 273.15) * 9 / 5 + 32).toFixed(1) + "°F"}
                    </p>
                </div>
                <div className="humidity">
                  <p>Humidity</p>
                  <p className="bold">{weatherData.main.humidity}%</p>
                </div>
                <div>
                  <p><FiSunrise /></p>
                  <p className="bold">{formatSunTime(weatherData.sys.sunrise)}</p>
                </div>
                <div>
                  <p><FiSunset /></p>
                  <p className="bold">{formatSunTime(weatherData.sys.sunset)}</p>
                </div>
              </div>
              <div className='conversion'>
                <h3 onClick={toggleTemperature}>
                <BsArrowRight /> Click me for {isCelsius ? "°F" : "°C"}
                </h3>
            </div>
            </div>
          )}
        </div>
      );
    }

    export default Home;
