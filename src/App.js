import './App.css';
import Greeting from './components/Greeting';
import Home from './pages/Home';
import Background from './components/Background';
import React from 'react';


function App() {

  return (
    <div className="App">
      <Home />
      <Background />
    </div>
  );
}

export default App;
